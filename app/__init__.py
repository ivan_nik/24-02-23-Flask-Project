from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os
from config import DevelopementConfig

app = Flask(__name__)  # Создаем приложение
DEBUG = True
app.config['SECRET_KEY'] = 'your_secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_URL', 'postgresql://postgres:test_password@localhost:5434/flask_db')



# БД
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Авторизация
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
app.app_context().push()

from . import views
